from django.db import models
from django.contrib.auth.models import AbstractUser

CATEGORY_CHOICES = (
    ('electronics', 'electronics'),
    ('fashion', 'fashion'),
    ('mobiles', 'mobiles'),
    ('grocery', 'grocery'),
    ('beauty', 'beauty')
)

PAYMENT_METHOD = (
    ('cod', 'cod'),
    ('credit_card', 'credit_card'),
    ('debit_card', 'debit_card'),
    ('upi', 'upi'),
    ('net_banking', 'net_banking')
)


class Users(AbstractUser):
    """
    User model
    field nam : Id, username, firstname, lastname, email, contact
    """
    username = models.CharField(max_length=264, unique=True)
    username.help_text = ''
    email = models.EmailField(unique=True, null=True)
    contact = models.CharField(max_length=13)


class Product(models.Model):
    """
    Product model:
    This will include all the product details
    """
    id = models.AutoField(primary_key=True, editable=False)
    user = models.ForeignKey(Users, on_delete=models.SET_NULL, null=True)
    product_name = models.CharField(max_length=200, null=True, blank=True)
    product_image = models.ImageField(null=True, blank=True)
    product_brand = models.CharField(max_length=200, null=True, blank=True)
    product_category = models.CharField(choices=CATEGORY_CHOICES, max_length=20, null=True, blank=True)
    product_description = models.TextField(null=True, blank=True)
    product_price = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return self.product_name


class Order(models.Model):
    """
    Order model:
    This will include all order details
    """
    user = models.ForeignKey(Users, on_delete=models.SET_NULL, null=True)
    payment_method = models.CharField(choices=PAYMENT_METHOD, max_length=20, null=True, blank=True)
    tax_price = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    shipping_price = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    total_price = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    is_paid = models.BooleanField(default=False)
    order_date = models.DateTimeField(auto_now_add=False, null=True, blank=True)
    id = models.AutoField(primary_key=True, editable=False)

    def __str__(self):
        return str(self.order_date)


class OrderItem(models.Model):
    """
    Order Item model:
    This will include particular item detail in order
    """
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True)
    product_name = models.CharField(max_length=150, blank=True, null=True)
    product_quantity = models.IntegerField(null=True, blank=True, default=0)
    product_price = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    id = models.AutoField(primary_key=True, editable=False)

    def __str__(self):
        return str(self.product_name)


class Cart(models.Model):
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)

