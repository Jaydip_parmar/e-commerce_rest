from django.contrib import admin
from django.contrib.auth import get_user_model
from .models import Order, Product, OrderItem
# Register your models here.

admin.site.register(get_user_model())
admin.site.register(Order)
admin.site.register(OrderItem)
admin.site.register(Product)
