# Generated by Django 3.2.9 on 2021-11-16 06:54

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ecommerce_app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('payment_method', models.CharField(blank=True, choices=[('cod', 'cod'), ('credit_card', 'credit_card'), ('debit_card', 'debit_card'), ('upi', 'upi'), ('net_banking', 'net_banking')], max_length=20, null=True)),
                ('tax_price', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True)),
                ('shipping_price', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True)),
                ('total_price', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True)),
                ('is_paid', models.BooleanField(default=False)),
                ('order_date', models.DateTimeField(blank=True, null=True)),
                ('id', models.AutoField(editable=False, primary_key=True, serialize=False)),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(editable=False, primary_key=True, serialize=False)),
                ('product_name', models.CharField(blank=True, max_length=200, null=True)),
                ('product_image', models.ImageField(blank=True, null=True, upload_to='')),
                ('product_brand', models.CharField(blank=True, max_length=200, null=True)),
                ('product_category', models.CharField(blank=True, choices=[('electronics', 'electronics'), ('fashion', 'fashion'), ('mobiles', 'mobiles'), ('grocery', 'grocery'), ('beauty', 'beauty')], max_length=20, null=True)),
                ('product_description', models.TextField(blank=True, null=True)),
                ('product_price', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True)),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('product_name', models.CharField(blank=True, max_length=150, null=True)),
                ('product_quantity', models.IntegerField(blank=True, default=0, null=True)),
                ('product_price', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True)),
                ('id', models.AutoField(editable=False, primary_key=True, serialize=False)),
                ('order', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='ecommerce_app.order')),
                ('product', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='ecommerce_app.product')),
            ],
        ),
    ]
